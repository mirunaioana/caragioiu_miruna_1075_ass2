const FIRST_NAME = "Miruna";
const LAST_NAME = "Caragioiu";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
    var o2 = {};
    var o = {
        pageAccessCounter: function(page = 'home') {
            var name = page.toLowerCase();
            if (o2[name]) {
                o2[name]++;
            } else {
                o2[name] = 1;
            }
        },
        getCache: function() {
            return o2;
        }
    };
    return o;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}